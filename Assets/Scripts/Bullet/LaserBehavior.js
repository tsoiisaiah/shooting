﻿#pragma strict


class LaserBehavior extends MonoBehaviour {
	private var line : LineRenderer;
	var hitEffectPrefab : GameObject;
	private var hitEffect : ParticleSystem;
	
	function Awake () {
		var hitEffectObject : GameObject = Instantiate (hitEffectPrefab, gameObject.transform.position , Quaternion.identity);
		hitEffect = hitEffectObject.GetComponent(ParticleSystem);
		hitEffect.Pause();
	}
	
	function Start () {
		line = gameObject.GetComponent(LineRenderer);
        line.enabled = false;
        
		line.SetPosition(0, Vector3.zero);
		line.SetPosition(1, Vector3.zero);
	}

	function Update () {
		if ((Input.GetKey ("x"))) {
			line.enabled = true;
			
			var ray : Ray = new Ray (transform.position, transform.up);
			var hit : RaycastHit;
			if (Physics.Raycast (ray, hit, Mathf.Infinity, 1 << 8)) {
				// sth hit
	//			Debug.Log("hit:" + hit.rigidbody.name);
				line.SetPosition(0, ray.origin);
				line.SetPosition(1, hit.point);
				
				hit.transform.SendMessage ("HitByLaser", 1f, SendMessageOptions.DontRequireReceiver);
				
				// hit effect
				if (hit.transform.gameObject.tag == "Monster") {
					hitEffect.transform.position = hit.point;
					hitEffect.Play();
				}
				else {
					// stop hit effect
					hitEffect.Pause();
					hitEffect.Clear();
				}
			}
			else {
				// nth hit...
				// not possible
				line.SetPosition(0, ray.origin);
				line.SetPosition(1, Vector3(0,Screen.height,0));
				
				// stop hit effect
				hitEffect.Pause();
				hitEffect.Clear();
			}
		}
		else {
			line.enabled = false;
			// stop hit effect
			hitEffect.Pause();
			hitEffect.Clear();
			
		}
	}
}