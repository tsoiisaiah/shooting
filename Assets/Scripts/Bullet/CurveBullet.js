﻿#pragma strict

// go forward, destroy on collided with ANY object
// do NOT penetrate

class CurveBullet extends BulletBehavior {

	var isMonsterBullet : boolean;
	var rotateRate : float;
	var noRotateAfterTIme : float;
	
	private var rotateStartTime : float;
	
	function Start () {
		rotateStartTime = Time.time;
	}

	// call by update, after the bullet is ready to fire
	function move () {
//		Debug.Log(Time.time + " - " + rotateStartTime + " < " + noRotateAfterTIme + "?");
		
		if (Time.time - rotateStartTime < noRotateAfterTIme) {
			transform.Rotate(0, 0, rotateRate * Time.deltaTime);
		}
		
		transform.Translate(Vector3.up * speed * Time.deltaTime);
	}

	// hit by sth
	function OnTriggerEnter (other : Collider) {
		if ((isMonsterBullet && other.tag == "Player") ||
			(!isMonsterBullet && other.tag == "Monster") ||
			other.tag == "BulletBlocker") {
			Destroy(gameObject);
		}
	}
	
}