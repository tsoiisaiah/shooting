﻿#pragma strict

// a cirle of defined bullet
// go forward, destroy on collided with ANY object
// do NOT penetrate

class CircleBulletGroup extends BulletBehavior {

	var bullet : GameObject;
	var bulletDefaultAngleAlongYAxis : float;
	var noOfBullet : int;
	var radius : float;
	
	private var bulletBehaviorArray = new Array();
	
	function Start () : void {	
		var adjustAngle : float = 360f / noOfBullet;
		var count : int = 0;
		
		for (var thisAngle : float = 0f; thisAngle < 360f; thisAngle += adjustAngle) {
//			Debug.Log("Group: creating bullet [" + ++count + "] on angle=" + thisAngle);
//			Debug.Log("Group: Quaternion " + bullet.transform.rotation);
		
			var thisBullet : GameObject = Instantiate (bullet, getPosition(thisAngle), Quaternion.Euler(0, 0, bulletDefaultAngleAlongYAxis + thisAngle));
			thisBullet.name = "bullet " + thisAngle;
			
			var bh : BulletBehavior = thisBullet.GetComponent(BulletBehavior);
			
//			Debug.Log("Group: Quaternion(new) " + thisBullet.transform.rotation);
			
			bulletBehaviorArray.Add(bh);
			
			// bullet rotate
//			bullet.transform.Rotate(Vector3(0, 0, adjustAngle));
		}
		
		// fire the bullet
		var bhList : BulletBehavior[] = bulletBehaviorArray.ToBuiltin(BulletBehavior) as BulletBehavior[];
		for(var i : int = 0; i < bhList.Length; i++) {
//			Debug.Log("Group: firing bullet[" + i + "]");
			
			// fire the bullet
			if (speed > 0) {
				// with custom speed
				bhList[i].fireWithSpeed(speed);
			}
			else {
				bhList[i].fire();
			}
			
		}
		
		// finish fire, delete
		Destroy(gameObject);
	}
	
	// return the position of the bullet with the corresponding angle
	function getPosition (angle : float) : Vector3 {
		var radian : float = angle * Mathf.Deg2Rad;
		
		var x : float;
		var y : float;
		
		x = radius * Mathf.Cos(radian);
		y = radius * Mathf.Sin(radian);
		
		return Vector3(x + transform.position.x, y + transform.position.y, transform.position.z);
	}
	
	// override
	function move () {
		// move original point
	}
	
	function fire () {
		// do not fire yet
	}
	
	function fireWithSpeed (speed : float) {
		this.speed = speed;
	}
	
}