﻿#pragma strict
// --------------
// StageManager
//   create stages in sequence
//   TODO
//   FSM as well?

public class LevelManager extends FSM {

	var stageList : Stage[];
	private var currStage : Stage;
	private var currIndex : int = 0;

	function Start () {
		Debug.Log("stage start");
		MessageGui.setTimeoutMessage("stage start", 2f);
		// next state
		setSingletonState(nextStage);
	}
	
	// ------------------------------
	// STATE
	// ------------------------------
	// run each stage one by one
	function startStage () : void {
		setState(wait);
		Debug.Log("--------------------------\n" +
				  "start stage: " + currIndex + "\n" +
				  "--------------------------");
	  	
		// start currStage and wait
		// attach to new gameobject
		var currStageObject = new GameObject(currStage.name);
		var currStageBehavior : StageBehavior = currStageObject.AddComponent(StageBehavior);
		
		if (currStage.type == Stage.Type.Boss) {
			MessageGui.setTimeoutMessage("--- Warning ---", 2f);
		}
		else {
			MessageGui.setTimeoutMessage(currStage.type + "-" + currStage.name, 2f);
		}
		
		currStageBehavior.init(currStage, nextStage);
	}
	
	function wait () : void {
		setState(null);
		Debug.Log("LevelManager sleep");
		// do nth
	}
	
	function nextStage () : void {
		// check if more stage exist
		// startStage for exist
		if (stageList.Length > currIndex) {
//			Debug.Log("next stage: " + currIndex);
			setSingletonState(startStage);
			currStage = stageList[currIndex++];
		}
		else {
			setSingletonState(completeLevel);
		}
	}
	
	// function loseStage () : void
	
	function completeLevel () : void {
		// clear level
		// show mark
		Debug.Log("win");
		MessageGui.setMessage("You Win");
	}
	
}