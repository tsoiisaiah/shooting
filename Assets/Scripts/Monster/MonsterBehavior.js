﻿#pragma strict

class MonsterBehavior extends FSM {

//	public var fsm : FSM;
	var hp : int;
	var bullet : GameObject;
	var explosionEffectPrefab : GameObject;
	
	private var hitEffect : GameObject;

	private var targetPosition : Vector3;
	private var exitPosition : Vector3;
	private var moveSpeed : float;
	private var stayTime : float;
	private var delaySpawnTime : float;
	
	private var fireRate : float;
	private var bulletSpeed : float;

	// being hit by sth
	function OnTriggerEnter (other : Collider) {
		if (other.CompareTag("Bullet")) {
//			Destroy(gameObject);
			TakeDamage(10);
		}
	}
	function HitByLaser (dmg : int) {
		// do dmg calculation later
//		Destroy(gameObject);
		TakeDamage(dmg);
	}
	function TakeDamage (dmg : int) {
		// take the dmg	
		hp -= dmg;
		if (hp <= 0) Die();
	}
	function Die () {
		// die effect
		ExplosionEffect();
		
		// destroy
		Destroy(gameObject);
	}
	
	// ------------------------------
	// INIT
	// variables to set before ready
	// ------------------------------
	public function setHp(hp : int) {
		this.hp = hp;
	}
	public function setTargetPosition (position : Vector3) {
		this.targetPosition = position;
	}
	public function setExitPosition (position : Vector3) {
		this.exitPosition = position;
	}
	public function setMoveSpeed (speed : float) {
		this.moveSpeed = speed;
	}
	public function setStayTime (stayTime : float) {
		this.stayTime = stayTime;
	}
	public function setBullet (bullet : GameObject) {
		this.bullet = bullet;
	}
	public function setBulletSpeed (bulletSpeed : float) {
		this.bulletSpeed = bulletSpeed;
	}
	public function setFireRate (fireRate : float) {
		this.fireRate = fireRate;
	}
	public function setDelaySpawnTime (delayTime : float) {
		this.delaySpawnTime = delayTime;
	}

	function setReady() : void {
		Debug.Log(name + " set Ready");
		if (delaySpawnTime <= 0) {
			setState(fadeIn);
		}
		else {
			setState(delaySpawn);
		}
	}
	
	// ------------------------------
	// OTHER FUNCTION
	// ------------------------------	
	// force the monster to fadeOut
	function forceEnd() : void {
		setState(fadeOut);
		Debug.Log(name + " is forceEnd");
	}
	
	// ------------------------------
	// STATE
	// ------------------------------
	private var delayStartTime : float = float.NaN;
	function delaySpawn() : void {
//		Debug.Log("delaySpawnTime = " + delaySpawnTime);
		
		if (float.IsNaN(delayStartTime)) {
			delayStartTime = Time.time;
		}
		else if (Time.time - delayStartTime >= delaySpawnTime) {
			Debug.Log(Time.time + " - " + delayStartTime + " >= " + delaySpawnTime);
			// fade in
			setState(fadeIn);
		}
	}
	
	function fadeIn() : void {
//		Debug.Log("fadeIn");
		var move = moveSpeed * Time.deltaTime;
		transform.position = Vector3.MoveTowards(transform.position, targetPosition, move);
		
		// reach
//		Debug.Log("compare: " + transform.position + " - " + targetPosition + " = " + transform.position.Equals(targetPosition));
		if (transform.position.Equals(targetPosition)) {
			setState(attackPlayer);
		}
	}

	private var startTime : float = float.NaN;
	private var lastBulletTime : float = 0;
//	var fireRate : float = 2;
	function attackPlayer() : void {
//		Debug.Log("attackPlayer");
		
		if (float.IsNaN(startTime)) {
			startTime = Time.time;
		}
		else if (Time.time - startTime >= stayTime) {
			// fade out
			setState(fadeOut);
		}
		
		// attack the player
		if (lastBulletTime == 0 || (Time.time - lastBulletTime) >= (1/fireRate)) {
			// allow to fire
			lastBulletTime = Time.time;
			var thisBullet : GameObject = Instantiate (bullet, transform.position, bullet.transform.rotation);
			var bh : BulletBehavior = thisBullet.GetComponent(BulletBehavior);;
			
			// fire the bullet
			if (bulletSpeed > 0) {
				// with custom speed
				bh.fireWithSpeed(bulletSpeed);
			}
			else {
				bh.fire();
			}
		}
	}

	function fadeOut() : void {
//		Debug.Log("fadeOut");
		var move = moveSpeed * Time.deltaTime;
		transform.position = Vector3.MoveTowards(transform.position, exitPosition, move);
		
		// reach
		if (transform.position.Equals(exitPosition)) {
			Destroy(gameObject);
		}
	}

	function ExplosionEffect () {
		Instantiate (explosionEffectPrefab, gameObject.transform.position, Quaternion.identity);
	}

}