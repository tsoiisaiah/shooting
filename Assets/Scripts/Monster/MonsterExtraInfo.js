﻿#pragma strict

public class MonsterExtraInfo {
	public var hp : int = 100;
	public var delaySpawnTime : float;
	public var stayTime : float;
	public var rotation : Quaternion;
	public var moveSpeed : float;
	public var bulletSpeed : float;
	public var fireRate : float;
	
	public var customBulletPrefab : GameObject;
	
	// location data
	// use custom position if set
	// custom(if set) > default(set) > random
	public var useCustomPosition : boolean;
	public var customSpawnPosition : Vector3;
	public var customTargetPosition : Vector3;
	public var customExitPosition : Vector3;
	public var spawnPosition : GameObject;
	public var targetPosition : GameObject;
	public var exitPosition : GameObject;
	
	public function getSpawnPosition () : Vector3 {
		if (useCustomPosition) {
			return customSpawnPosition;
		}
		else {
			return spawnPosition.transform.position;
		}
	}
	public function getTargetPosition () : Vector3 {
		if (useCustomPosition) {
			return customTargetPosition;
		}
		else {
			return targetPosition.transform.position;
		}
	}
	public function getExitPosition () : Vector3 {
		if (useCustomPosition) {
			return customExitPosition;
		}
		else {
			return exitPosition.transform.position;
		}
	}
	
}