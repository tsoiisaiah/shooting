﻿#pragma strict

private var thisParticleSystem : ParticleSystem;
function Start () {
	thisParticleSystem = GetComponent(ParticleSystem);
}

function LateUpdate () {
	if (!thisParticleSystem.IsAlive()) {
		Destroy (gameObject);
	}
}