﻿#pragma strict

public var bullet : GameObject;
public var speed : float;
public var fireRate : float;
public var screenBorder : float;

private var lastBulletTime : float = 0;

function Start () {
//	line = gameObject.GetComponent(LineRenderer);
//	line.enabled = false;
}

function Update () {

	// movement
	if ((Input.GetKey ("up")) || (Input.GetKey ("w"))) {
//		gameObject.transform.Translate(Vector3.up * speed * Time.deltaTime);
		move(Vector3.up, speed * Time.deltaTime);
	}
	else if ((Input.GetKey ("down")) || (Input.GetKey ("s"))) {
//		gameObject.transform.Translate(Vector3.down * speed * Time.deltaTime);
		move(Vector3.down, speed * Time.deltaTime);
	}
	else if ((Input.GetKey ("left")) || (Input.GetKey ("a"))) {
//		gameObject.transform.Translate(Vector3.left * speed * Time.deltaTime);
		move(Vector3.left, speed * Time.deltaTime);
	}
	else if ((Input.GetKey ("right")) || (Input.GetKey ("d"))) {
//		gameObject.transform.Translate(Vector3.right * speed * Time.deltaTime);
		move(Vector3.right, speed * Time.deltaTime);
	}
	
	// shoot
	if ((Input.GetKey ("space"))) {
//		Debug.Log("lastBulletTime=" + lastBulletTime + "; currTime=" + Time.time);
		if (lastBulletTime == 0 || (Time.time - lastBulletTime) >= (1/fireRate)) {
			// allow to fire
			lastBulletTime = Time.time;
			var thisBullet : GameObject = Instantiate (bullet, transform.position, bullet.transform.rotation);
			var thisBulletBehavior : BulletBehavior = thisBullet.GetComponent(BulletBehavior);
			thisBulletBehavior.fire();
		}
	}
	
}

function move (dir : Vector3, dist : float) {
	gameObject.transform.Translate(dir * dist);
	
	// adjust position
	var viewPosition : Vector3 = Camera.main.WorldToScreenPoint(gameObject.transform.position);

	if (viewPosition.x > Screen.width-screenBorder) {
		viewPosition.x = Screen.width-screenBorder;
	}
	else if (viewPosition.x < screenBorder) {
		viewPosition.x = screenBorder;
	}
	else if (viewPosition.y > Screen.height-screenBorder) {
		viewPosition.y = Screen.height-screenBorder;
	}
	else if (viewPosition.y < screenBorder) {
		viewPosition.y = screenBorder;
	}
	
	gameObject.transform.position = Camera.main.ScreenToWorldPoint (viewPosition);
}