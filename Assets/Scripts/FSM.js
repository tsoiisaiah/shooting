﻿#pragma strict

class FSM extends MonoBehaviour {
    private var activeState : Function; // points to the currently active state function
    private var lastState : Function;
    private var isSingletonState : boolean;

	function Start () {
		// nth
	}

	function Update () {
        if (activeState != null && (isSingletonState?(activeState != lastState):true)) {
        	lastState = activeState;
            activeState();
        }
	}
	
    public function setState(state : Function) :void {
        activeState = state;
        isSingletonState = false;
    }
    
    public function setSingletonState(state : Function) :void {
    	activeState = state;
    	isSingletonState = true;
    }
	
}