﻿#pragma strict

class MonsterPair extends System.Object {
	var monsterPrefab : GameObject;
	var extraInfo : MonsterExtraInfo[];

	public function Pair (x : GameObject, y : MonsterExtraInfo[]) {
		monsterPrefab = x;
		extraInfo = y;
	}

	public function setFirst (x : GameObject) : void {
		monsterPrefab = x;
	}
	public function setSecond (y : MonsterExtraInfo[]) : void {
		extraInfo = y;
	}

	public function getFirst () : GameObject {
		return monsterPrefab;
	}
	public function getSecond () : MonsterExtraInfo[] {
		return extraInfo;
	}

	public function ToString () : String {
		return "Pair(" + monsterPrefab.name + ", " + extraInfo + ")";
	}
}