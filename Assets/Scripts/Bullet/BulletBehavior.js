﻿#pragma strict

// abstract class

class BulletBehavior extends MonoBehaviour {
	public var speed : float;
	public var acceleration : float;
	public var readyToFire : boolean = false;
	
	function Update () {
		if (readyToFire) {
		
			// update the speed
			if (!float.IsNaN(acceleration)) {
				speed += acceleration;
			}
		
			// move the bullet
			move();
		}
	}
	
	function move() {
		Debug.Log("Warning: fire function not implement for bullet [" + name + "]");
	}

	// hit by sth
	function OnTriggerEnter (other : Collider) {
		Debug.Log("Warning: OnTriggerStay function not implement for bullet [" + name + "]");
	}
	
	// fire the bullet!
	function fire () {
		this.readyToFire = true;
	}
	
	function fireWithSpeed (speed : float) {
		this.speed = speed;
		this.readyToFire = true;
	}
	
	function setAcceleration (acceleration : float) {
		this.acceleration = acceleration;
	}
	
}