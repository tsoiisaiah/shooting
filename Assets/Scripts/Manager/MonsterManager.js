﻿#pragma strict

var spawnPositionList : GameObject[];
var attackPositionList : GameObject[];
var exitPositionList : GameObject[];

public var monsterPrefab : GameObject;
public var moveSpeed : float;
public var monsterBullet : GameObject;

function Start () {
	// testing code
	// create a test monster
	spawnPositionList = GameObject.FindGameObjectsWithTag ("spawnPosition");
	attackPositionList = GameObject.FindGameObjectsWithTag ("attackPosition");
	exitPositionList = GameObject.FindGameObjectsWithTag ("exitPosition");
	
	var sp = Random.Range(0,spawnPositionList.Length);
	var ap = Random.Range(0,attackPositionList.Length);
	var ep = Random.Range(0,exitPositionList.Length);
	
	var monster : GameObject = Instantiate (monsterPrefab, spawnPositionList[sp].transform.position, spawnPositionList[sp].transform.rotation);
	var monsterBehavior : MonsterBehavior = monster.GetComponent(MonsterBehavior);
	
	monsterBehavior.setTargetPosition(attackPositionList[ap].transform.position);
	monsterBehavior.setExitPosition(exitPositionList[ep].transform.position);
	monsterBehavior.setMoveSpeed(moveSpeed);
	monsterBehavior.setBullet(monsterBullet);
	monsterBehavior.setReady();
}

function Update () {
	
}


// state
function initStage() : void {
	
}

function clearStage() : void {
	
}

