﻿#pragma strict

class StageBehavior extends FSM {
	@HideInInspector
	public var spawnPositionList : GameObject[];
	@HideInInspector
	public var attackPositionList : GameObject[];
	@HideInInspector
	public var exitPositionList : GameObject[];

	var type : Stage.Type;
	var maxTime : float;
	var startDelayTime : float;
	var endDelayTime : float;
	
	var monsterList : MonsterPair[];
	private var monsterBehaviorArray = new Array();
	// list of monsterBehavior
	var mbList : MonsterBehavior[];
	
	// tell stageManager to next stage
	var callBackOnComplete : Function;
	
	function start () {
		spawnPositionList = GameObject.FindGameObjectsWithTag ("spawnPosition");
		attackPositionList = GameObject.FindGameObjectsWithTag ("attackPosition");
		exitPositionList = GameObject.FindGameObjectsWithTag ("exitPosition");
		
	}
	
	// init the stage
	public function init (data : Stage, callBackOnComplete : Function) : void {
		setName(data.name);
		setType(data.type);
		setMaxTime(data.maxTime);
		setStartDelayTime(data.startDelayTime);
		setEndDelayTime(data.endDelayTime);
		setMonsterList(data.monsterList);
		
		setCallBackOnComplete(callBackOnComplete);
		
		setReady();
	}
	
	// variables to set before ready
	public function setName(stageName : String) : void {
		name = stageName;
	}
	public function setType(type : Stage.Type) : void {
		this.type = type;
	}
	public function setMaxTime(maxTime : float) : void {
		this.maxTime = maxTime;
	}
	private function setStartDelayTime (startDelayTime : float) : void {
		this.startDelayTime = startDelayTime;
	}
	private function setEndDelayTime (endDelayTime : float) : void {
		this.endDelayTime = endDelayTime;
	}
	public function setMonsterList(monsterList : MonsterPair[]) : void {
		this.monsterList = monsterList;
	}
	public function setCallBackOnComplete(callBackOnComplete : Function) : void {
		this.callBackOnComplete = callBackOnComplete;
	}

	function setReady() : void {
		Debug.Log(name + " set Ready");
		
		if (startDelayTime > 0) {
			Debug.Log(name + " waiting initStage for " + startDelayTime);
			waitAndGotoState(startDelayTime, initStage, true);
		}
		else {
			setState(initStage);
		}
	}
	
	function waitAndGotoState(waitTime : float, stateAfterWait : Function, isSingletonStateAfterWait : boolean) {
		this.waitTime = waitTime;
		this.stateAfterWait =  stateAfterWait;
		this.isSingletonStateAfterWait = isSingletonStateAfterWait;
		
		setState(wait);
	}
	
	// ------------------------------
	// STATE
	// ------------------------------
	var waitStartTime : float;
	var waitTime : float;
	var stateAfterWait : Function;
	var isSingletonStateAfterWait : boolean;
	function wait () {
//		Debug.Log("waiting for " + waitTime);
		// but y it become 0...?
		if (float.IsNaN(waitStartTime) || waitStartTime == 0) {
			waitStartTime = Time.time;
		}
		else if (Time.time - waitStartTime >= waitTime) {
			Debug.Log(name + ": " + Time.time + " - " + waitStartTime + " >= " + waitTime);
			if (isSingletonStateAfterWait) {
				setSingletonState(stateAfterWait);
			}
			else {
				setState(stateAfterWait);
			}
			
			waitStartTime = float.NaN;
			waitTime = float.NaN;
			stateAfterWait = null;
		}
	}
	
	// init the stage
	function initStage () : void {
		// may hv sth to do here
		
		setSingletonState(createAllMonster);
	}
	
	function createAllMonster () : void {
		Debug.Log(name + " waiting endStage for " + maxTime);
		waitAndGotoState(maxTime, endStage, true);
		
		Debug.Log("createAllMonster");
		
		// init all monster
		for(var i : int = 0; i < monsterList.Length; i++) {
			var pair : MonsterPair = monsterList[i];
			createMonsters(pair.getFirst(), pair.getSecond());
		}
		
		mbList = monsterBehaviorArray.ToBuiltin(MonsterBehavior) as MonsterBehavior[];
		// set all monster to ready at the same time(almost)
		for(i = 0; i < mbList.Length; i++) {
			mbList[i].setReady();
		}
		
	}
	
//	var startTime : float = float.NaN;
//	function wait () : void {
//		// no max time, wait forever
//		if (maxTime == 0.0) {
//			setState(null);
//		}
//		// start time
//		else if (float.IsNaN(startTime)) {
//			startTime = Time.time;
//		}
//		// wait until max time reach
//		// to endStage after maxTime
//		else if (Time.time - startTime > maxTime) {
//			setSingletonState(endStage);
//		}
//		
//	}
	
	function endStage () : void {
		// finish the stage
		for(var i : int = 0; i < mbList.Length; i++) {
			if (mbList[i] != null) {
				mbList[i].forceEnd();
			}
		}
		
		Debug.Log("Stage complete: " + name);
		
		// complete
		Debug.Log(name + " waiting complete for " + endDelayTime);
		waitAndGotoState(endDelayTime, complete, true);
//		setSingletonState(complete);
	}
	
	function complete () : void {
		// tell stageManager this stage is complete
		setSingletonState(destroyState);
		callBackOnComplete();
	}
	
	function destroyState () : void {
		Destroy(this);
	}
	
	// create monsters
	function createMonsters(monster : GameObject, extraInfos : MonsterExtraInfo[]) : void {		
		for (var extraInfo : MonsterExtraInfo in extraInfos) {
			createMonster(monster, extraInfo);
		}
	}
	
	function createMonster(monsterPrefab : GameObject, extraInfo : MonsterExtraInfo) : void {
		var rotation : Quaternion = extraInfo.rotation;
//		if (rotation != Quaternion(0,0,0,0) {
			rotation = monsterPrefab.transform.rotation;
//		}
		
		// create the monster
		var monster : GameObject = Instantiate (monsterPrefab, extraInfo.getSpawnPosition(), rotation);
		var monsterBehavior : MonsterBehavior = monster.GetComponent(MonsterBehavior);
		
		// update info of the monster
		monsterBehavior.setHp(extraInfo.hp);
		monsterBehavior.setTargetPosition(extraInfo.getTargetPosition());
		monsterBehavior.setExitPosition(extraInfo.getExitPosition());
		monsterBehavior.setStayTime(extraInfo.stayTime);
		monsterBehavior.setDelaySpawnTime(extraInfo.delaySpawnTime);
		
		monsterBehavior.setMoveSpeed(extraInfo.moveSpeed);
		// bullet
		if (extraInfo.customBulletPrefab != null) {
			monsterBehavior.setBullet(extraInfo.customBulletPrefab);
		}
		
		// bullet prop
		monsterBehavior.setBulletSpeed(extraInfo.bulletSpeed);
		monsterBehavior.setFireRate(extraInfo.fireRate);
		
//		monsterBehaviorList[index++] = monsterBehavior;
		monsterBehaviorArray.Add(monsterBehavior);
	}
	
}