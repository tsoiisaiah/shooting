﻿#pragma strict


public class MessageGui extends MonoBehaviour {
	public var GuiSkin : GUISkin;

	public var messageBackground : Texture2D;
	public var x : float;
	public var y : float;
	public var height : float;
	public var fontSize : int;
	public var defaultMessage : String;

	// message to print
	static var message : String = "";
	static var messageTimeoutTime : float;
	static var messageStartTime : float;

	function Start () {
		if (message.Equals("")) message = defaultMessage;
//		Debug.Log("draw [" + message + "]");
	}

	function Update () {
		if ((messageTimeoutTime > 0f) && (Time.time > messageStartTime + messageTimeoutTime)) {
			removeMessage();
		}
	}
	function OnGUI() {
		// print mesage only if there are message to show
//		Debug.Log("draw [" + message + "]");
		if (!message.Equals("")) {
			GUI.skin = GuiSkin;

			var r : Rect = Rect (DimX(x), DimY(y), Screen.width, DimY(height));
			GUI.DrawTexture (r, messageBackground);
			
			GUI.skin.label.fontSize = DimX(fontSize);
			GUI.Label(Rect(r.x, r. y, r.width, r.height), message);
//			Gu.Label(Gu.Dim(x), Gu.Dim(y), fontSize, message);
		}
	}

	static function setMessage (msg : String) {
		removeMessage();
		message = msg;
	}
	static function removeMessage() {
		messageStartTime = 0f;
		messageTimeoutTime = 0f;
		message = "";
	}

//	static var maxMessageCount : int = 6;
//	static List<string> msgList = new List<string>();
//	public static void appendMessage (string msg) {
//		msgList.Add (msg);
//
//		while (msgList.Count > maxMessageCount) {
//			msgList.RemoveAt(0);
//		}
//
//		message = "";
//		for (int i = 0; i < msgList.Count; i++) {
//			message += msgList[i] + "\n";
//		}
//
//	}

	static function setTimeoutMessage (msg : String, time : float) {
		message = msg;
		messageStartTime = Time.time;
		messageTimeoutTime = time;
	}

	function DimX(percent : float) : int {
		return (percent/100 * Screen.width);
	}
	function DimY(percent : float) : int {
		return (percent/100 * Screen.height);
	}
}