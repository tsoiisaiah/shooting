﻿#pragma strict

// go forward, destroy on collided with ANY object
// do NOT penetrate

class ForwardBullet extends BulletBehavior {

	var isMonsterBullet : boolean;

	// call by update, after the bullet is ready to fire
	function move () {
		transform.Translate(Vector3.up * speed * Time.deltaTime);
	}

	// hit by sth
	function OnTriggerEnter (other : Collider) {
		if ((isMonsterBullet && other.tag == "Player") ||
			(!isMonsterBullet && other.tag == "Monster") ||
			other.tag == "BulletBlocker") {
			Destroy(gameObject);
		}
	}
	
}