﻿#pragma strict

// stage is created by StageManager
class Stage extends System.Object {
	enum Type{Normal, Boss};
	
	var name : String;
	var type : Type;
	var maxTime : float;
	
	var startDelayTime : float;
	var endDelayTime : float;
	
	var monsterList : MonsterPair[];
		
}